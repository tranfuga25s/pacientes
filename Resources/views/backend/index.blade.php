@extends ('backend.layouts.app')

@section ('title', trans('paciente::labels.backend.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('paciente::labels.backend.management') }}
        <small>{{ trans('paciente::labels.backend.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('paciente::labels.backend.active') }}</h3>

            <div class="box-tools pull-right">
                <div class="pull-right mb-10 hidden-sm hidden-xs">
                    {{ link_to_route('admin.pacientes.index', trans('paciente::menus.backend.all'), [], ['class' => 'btn btn-primary btn-xs']) }}
                    {{ link_to_route('admin.pacientes.create', trans('paciente::menus.backend.create'), [], ['class' => 'btn btn-success btn-xs']) }}
                    {{ link_to_route('admin.pacientes.deactivated', trans('paciente::menus.backend.deactivated'), [], ['class' => 'btn btn-warning btn-xs']) }}
                    {{ link_to_route('admin.pacientes.deleted', trans('paciente::menus.backend.deleted'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull right-->

                <div class="pull-right mb-10 hidden-lg hidden-md">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu" role="menu">
                            <li>{{ link_to_route('admin.access.user.index', trans('menus.backend.access.users.all')) }}</li>
                            <li>{{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create')) }}</li>
                            <li class="divider"></li>
                            <li>{{ link_to_route('admin.access.user.deactivated', trans('menus.backend.access.users.deactivated')) }}</li>
                            <li>{{ link_to_route('admin.access.user.deleted', trans('menus.backend.access.users.deleted')) }}</li>
                        </ul>
                    </div><!--btn group-->
                </div><!--pull right-->

                <div class="clearfix"></div>
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="pacientes-table" 
                       class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('paciente::labels.backend.table.id') }}</th>
                        <th>{{ trans('paciente::labels.backend.table.first_name') }}</th>
                        <th>{{ trans('paciente::labels.backend.table.last_name') }}</th>
                        <th>{{ trans('paciente::labels.backend.table.email') }}</th>
                        <th>{{ trans('paciente::labels.backend.table.created') }}</th>
                        <th>{{ trans('paciente::labels.backend.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('paciente::history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" 
                        data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Paciente') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}

    <script>
        $(function () {
            $('#pacientes-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.pacientes.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false}
                },
                columns: [
                    {data: 'id', name: 'paciente.id'},
                    {data: 'first_name', name: '{{config('access.users_table')}}.first_name'},
                    {data: 'last_name', name: '{{config('access.users_table')}}.last_name'},
                    {data: 'email', name: '{{config('access.users_table')}}.email'},
                    {data: 'updated_at', name: '{{config('access.users_table')}}.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection
