@extends('frontend.layouts.app')

@section('content')
    <h1>Hello World</h1>
    <div class="panel panel-default">
        <div class="panel-heading">
            Current pacients
        </div>
        <div class="panel-body">
            <ul class="list-simple">
                @foreach ($pacientes as $paciente)
                <li>{{ $paciente->usuario->email }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
