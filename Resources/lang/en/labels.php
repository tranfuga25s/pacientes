<?php
return [
    'backend' => [
        'management' => "Pacient list",
        'active' => "Active pacients",
        
        'table' => [
            'id' => "#ID",
            'first_name' => "First name",
            'last_name' => "Last name",
            'email' => "Email",
            'created' => "Created",
            'last_updated' => "Last update"
        ]
    ]
];

