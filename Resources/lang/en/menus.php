<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'paciente' => [
            'title' => 'Pacients',

        ],
        'all' => "All pacients",
        'create' => "Create a pacient",
        'deactivate' => "Deactivate a pacient",
        'deleted' => "Delete a pacient"
    ],
];
