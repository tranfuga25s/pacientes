<?php
return [
    'backend' => [
        'management' => "Lista de pacientes",
        'active' => "Pacientes Activos",
        
        'table' => [
            'id' => "#ID",
            'first_name' => "Nombres",
            'last_name' => "Apellidos",
            'email' => "Email",
            'created' => "Creado",
            'last_updated' => "Ultima actualizacion"
        ]
    ]
];

