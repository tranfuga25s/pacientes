<?php

namespace Modules\Paciente\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PacienteDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('pacientes')->insert([
            'user_id'    => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
        ]);
        Model::reguard();
    }
}
