<?php

Route::group([
    'middleware' => 'web', 
    'prefix' => 'paciente', 
    'namespace' => 'Modules\Paciente\Http\Controllers\Frontend'
 ], function() {
    Route::get('/', 'PacienteController@index');
});

// Backend Routes
Route::group([
    'middleware' => 'web',
    'prefix' => 'admin/pacientes',
    'namespace' => 'Modules\Paciente\Http\Controllers\Backend'
    ], function() {
    
    Route::get(
        '/index', 
        'PacientesController@index'
    )->name('admin.pacientes.index');
    
    Route::get(
        '/list',
        'PacientesController@invoke'
    )->name('admin.pacientes.get');
    
});
