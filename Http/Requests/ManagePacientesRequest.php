<?php

namespace Modules\Paciente\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest.
 */
class ManagePacientesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }//end authorize()


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];

    }//end rules()


}//end class
