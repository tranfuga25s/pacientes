<?php

namespace Modules\Paciente\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use Modules\Paciente\Repositories\PacientesRepository;
use Illuminate\Http\Request;
use Modules\Paciente\Http\Requests\ManagePacientesRequest;

class PacientesController extends Controller {
    
    /**
     * @var PacientesRepository
     */
    protected $pacientes;

    /**
     * @param PacientesRepository $pacientes
     */
    public function __construct(PacientesRepository $pacientes)
    {
        $this->pacientes = $pacientes;
    }
    
    public function index(Request $req) {
        $pacientes = $this->pacientes->getAll();

        return view('paciente::backend.index')
               ->withResults($pacientes);
    }
    
    /**
     * @param ManagePacientesRequest $request
     *
     * @return mixed
     */
    public function listing(ManagePacientesRequest $request)
    {
        return Datatables::of($this->pacientes->getForDataTable($request->get('trashed')))
        //->escapeColumns(['razon_social'])
        ->addColumn('actions', function ($paciente) {
            return [];
        })
        ->withTrashed()
        ->make(true);
    }
    
    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function create(ManagePacientesRequest $request)
    {
        return view('paciente::backend.create');
    }//end create()
    
    /**
     * @param StoreUserRequest $request
     *
     * @return mixed
     */
    public function store(StorePacienteRequest $request)
    {
        return redirect()
                ->route('admin.pacientes.index')
                ->withFlashError("Not implemented yet");
        
        $this->pacientes->create(
            [
                'data'  => $request->only(
                    'first_name',
                    'last_name',
                    'email',
                    'password',
                    'status',
                    'confirmed',
                    'confirmation_email'
                )
            ]
        );

        return redirect()
                ->route('admin.pacientes.index')
                ->withFlashSuccess(trans('paciente::alerts.backend.created'));

    }//end store()


    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function show(User $user, ManageUserRequest $request)
    {
        return view('backend.access.show')
            ->withUser($user);

    }//end show()


    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request)
    {
        return view('backend.access.edit')
            ->withUser($user)
            ->withUserRoles($user->roles->pluck('id')->all())
            ->withRoles($this->roles->getAll());

    }//end edit()


    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->users->update(
            $user,
            [
             'data'  => $request->only(
                 'first_name',
                 'last_name',
                 'email',
                 'status',
                 'confirmed'
             ),
             'roles' => $request->only('assignees_roles'),
            ]
        );

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));

    }//end update()


    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function destroy(User $user, ManageUserRequest $request)
    {
        $this->users->delete($user);

        return redirect()->route('admin.access.user.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted'));

    }//end destroy()

}

