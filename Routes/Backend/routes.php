<?php

Route::group(
  [
    'prefix' => 'pacientes',
    'as'     => 'pacientes.',
    'middleware' => [
        'admin',
        'access.routeNeedsRole:1'
    ],
  ], function() {
    Route::get(
      '/',
      '\Modules\Paciente\Http\Controllers\Backend\PacientesController@index'
    )->name('index');

    Route::post(
      '/list',
      '\Modules\Paciente\Http\Controllers\Backend\PacientesController@listing'
    )->name('get');
    
    Route::post(
      '/deactivated',
      '\Modules\Paciente\Http\Controllers\Backend\PacientesController@listing'
    )->name('deactivated');
    
    Route::post(
      '/deleted',
      '\Modules\Paciente\Http\Controllers\Backend\PacientesController@listing'
    )->name('deleted');
    
    // Create
    Route::get(
        '/create',
        '\Modules\Paciente\Http\Controllers\Backend\PacientesController@create'
    )->name('create');
    
    Route::post(
        '/store',
        '\Modules\Paciente\Http\Controllers\Backend\PacientesController@store'
    )->name('store');
  });
