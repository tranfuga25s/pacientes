<?php
if (!Breadcrumbs::exists('admin.paciente')) {
    Breadcrumbs::register('admin.paciente', function($breadcrumbs) {
        $breadcrumbs->parent('admin.dashboard');
        $breadcrumbs->push(
            trans('paciente:labels.backend.paciente'), 
            route('admin.paciente.index')
        );
    });
}

