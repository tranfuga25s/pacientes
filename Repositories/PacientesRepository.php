<?php

namespace Modules\Paciente\Repositories;

use App\Repositories\BaseRepository;
use Modules\Paciente\Entities\Paciente;

class PacientesRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Paciente::class;

    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false) {
        /*
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
                ->joinWhere('users', 'users.id', '=', 'paciente.user_id')
                ->select(
                [
                    'pacientes.id',
                    config('access.users_table') . '.first_name',
                    config('access.users_table') . '.last_name',
                    config('access.users_table') . '.email',
                    config('access.users_table') . '.status',
                    config('access.users_table') . '.confirmed',
                    'pacientes.created_at',
                    'pacientes.updated_at',
                    'pacientes.deleted_at',
                ]
        );

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

//end getForDataTable()
}
